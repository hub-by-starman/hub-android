package hudson.launch

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import hudson.R
import hudson.pair.PairReadyActivity
import kotlinx.android.synthetic.main.launch_activity_cert_error.*


class CertConfigErrorActivity : AppCompatActivity() {
	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}

	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			errorLayout.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
		}
		else{
			errorLayout.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
		}
	}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.launch_activity_cert_error)
		changeBackground()
		val extras = intent.extras
		var passedErrorMessage = ""
		if (extras != null) {
			passedErrorMessage = extras.getString("error")!!
		}


		errorMessage.text = passedErrorMessage


		pairButton.setOnClickListener {
            val intent = Intent(this, PairReadyActivity::class.java)
            this.startActivity(intent)
            finish()
        }
    }
}
