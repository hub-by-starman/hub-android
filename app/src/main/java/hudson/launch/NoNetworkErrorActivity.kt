package hudson.launch

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import hudson.R
import kotlinx.android.synthetic.main.launch_activity_no_internet.*


class NoNetworkErrorActivity : AppCompatActivity() {

	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}
	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			noInternetLayout.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
		}
		else{
			noInternetLayout.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
		}
	}
    @TargetApi(Build.VERSION_CODES.M)
	@RequiresApi(Build.VERSION_CODES.M)
	override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.launch_activity_no_internet)
		changeBackground()
        val animatedIcon = sad_face_no_internet
        val iconAvd = animatedIcon.drawable as AnimatedVectorDrawable
        iconAvd.registerAnimationCallback(object : Animatable2.AnimationCallback() {})
        iconAvd.start()
    }


	override fun onResume() {
		super.onResume()
		val filter = IntentFilter()
		filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
		registerReceiver(networkStateReceiver, filter)
	}

	override fun onPause() {
		super.onPause()
		unregisterReceiver(networkStateReceiver)
	}

	private var networkStateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			Log.d("app", "Network connectivity change")
			if (intent.extras != null) {
				val networkInfo = intent.extras!!.get(ConnectivityManager.EXTRA_NETWORK_INFO) as NetworkInfo
				if (networkInfo.state == NetworkInfo.State.CONNECTED) {

					val i = Intent(context.applicationContext, LoadingActivity::class.java)
					i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
					context.startActivity(i)
					finish()

				}
			}
			if (intent.extras!!.getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, java.lang.Boolean.FALSE)) {
				Log.d("app", "There's no network connectivity")
			}
		}
	}

}
