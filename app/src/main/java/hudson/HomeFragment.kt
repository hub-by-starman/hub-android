package hudson

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hudson.R
import hudson.launch.AppDatabase
import hudson.launch.LoadingActivity
import kotlinx.android.synthetic.main.main_fragment_home.view.*
import kotlin.concurrent.thread


class HomeFragment: Fragment() {

    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }

    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait1)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape1)
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.main_fragment_home, container, false)
        changeBackground()
        val db = AppDatabase.getInstance(this.context!!)
		rootView!!.textView12.text = String.format(resources.getString(R.string.you_re_logged_in_as), LoadingActivity.loggedInUser)

        rootView!!.logOutButton.setOnClickListener {
            thread(start=true){
                db?.loginDAO()?.resetLoggedInUser()
                val intent = Intent(activity, LoadingActivity::class.java)
                this.startActivity(intent)
                activity?.finish()
            }
        }

        return rootView
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}