package hudson.user

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.beust.klaxon.JsonArray
import hudson.R
import kotlinx.android.synthetic.main.user_activity.*

class UserActivity : AppCompatActivity() {

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        setContentView(R.layout.user_activity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_activity)
		//userList.adapter.notifyDataSetChanged()
        //getUsers()
        viewPage = container

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

		//Log.d("TESTING", userList.toString())
		//userList.adapter.notifyDataSetChanged()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

	data class UsersData(val content: String) {
		override fun toString(): String = content
	}


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        companion object {
            var fragmentSize: Int = 1
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 // Fragment # 0
                -> UserSelect.newInstance(position + 1)
                else // Fragment # 4
                //-> UserRegisterLogin.newInstance(position + 1)
                -> UserRegisterLogin.newInstance(position + 1)

            }
        }
        override fun getCount(): Int {
            // Show 2 total pages.
            return fragmentSize
        }

    }
    companion object {
        var usersArray: JsonArray<*>? = null
		var users: MutableList<UsersData> = ArrayList()
		var viewPage: ViewPager? = null
        var mSectionsPagerAdapter: SectionsPagerAdapter? = null
        var formattedUserList: MutableList<String> = mutableListOf()

    }


}
