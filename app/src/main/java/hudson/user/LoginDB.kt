package hudson.user

import android.arch.persistence.room.*


@Entity(tableName = "loginInfo")
class LoginDB {
    @PrimaryKey
    var token: String = ""
    var user: String = ""
    var loggedIn: Int = 0
}


@Dao
interface LoginDBInterface {

    @Query("SELECT * from loginInfo")
    fun getAll(): List<LoginDB>

    @Query("SELECT * from loginInfo WHERE user= :user")
    fun getUser(user: String): List<LoginDB>

    @Insert
    fun updateLoggedInUser(loginValues: LoginDB)

    @Query("UPDATE loginInfo SET loggedIn= 0 WHERE loggedIn=1")
    fun resetLoggedInUser()

    @Query("SELECT * from loginInfo WHERE loggedIn=1")
    fun getLoggedInUser(): List<LoginDB>

    @Query("UPDATE loginInfo SET loggedIn= 1 WHERE user= :user")
    fun loginExistingUser(user: String)

}
