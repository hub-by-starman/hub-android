package hudson.user


import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.core.Json
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import hudson.R
import hudson.launch.AppDatabase
import hudson.launch.CertConfigErrorActivity
import hudson.launch.LoadingActivity
import hudson.user.UserActivity.Companion.formattedUserList
import hudson.user.UserSelect.Companion.loggingIn
import hudson.user.UserSelectAdapter.Companion.currentHasPassword
import hudson.user.UserSelectAdapter.Companion.currentUserInDB
import kotlinx.android.synthetic.main.user_fragment_login_register.*
import kotlinx.android.synthetic.main.user_fragment_login_register.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.lang.Exception
import kotlin.concurrent.thread


class UserRegisterLogin : Fragment() {



    private var usernameEntered: Boolean = false
    private var rootView: View? = null

    override fun onConfigurationChanged(newConfig: Configuration){
        super.onConfigurationChanged(newConfig)
        changeBackground()
    }

    private fun changeBackground(){
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadportrait2)
        }
        else{
            rootView!!.setBackgroundResource(R.drawable.ic_backgroundloadlandscape2)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            Log.d("TESTING", "Login/Register Screen is visible")


            if (loggingIn){
                Log.d("TESTING", "User is logging in")
                when {
                    currentUserInDB -> {
                        Log.d("TESTING", "User has a password")
                        enterWifiTopText.text = String.format(resources.getString(R.string.welcome_back), username.toString())
                        loginUsernameInput.visibility = GONE
                        registerLoginButtonText.text = resources.getString(R.string.login)

                        Log.d("TESTING", "User is in the Database already")
                        enterWifiTopText.visibility = GONE
                        loginUsernameInputLayout.visibility = GONE
                        loginPasswordInputLayout.visibility = GONE
                        loggingYouInText.visibility = VISIBLE
                        thread(start=true) {

                            val (_, _, tokenCheckResult) = Fuel.get("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/loginCheck/").header(mapOf("Authorization" to "token ${UserSelectAdapter.currentUserToken}")).responseJson()
                            handleTokenCheck(tokenCheckResult)
                        }
                    }
                    currentHasPassword -> {
                        Log.d("TESTING", "User has a password")
                        enterWifiTopText.text = String.format(resources.getString(R.string.welcome_back), username.toString())
                        loginUsernameInput.visibility = GONE
                        registerLoginButtonText.text = resources.getString(R.string.login)
                    }
                    else -> {
                        Log.d("TESTING", "User has no password")
                        enterWifiTopText.visibility = GONE
                        loginUsernameInputLayout.visibility = GONE
                        loginPasswordInputLayout.visibility = GONE
                        loggingYouInText.visibility = VISIBLE
                        thread(start=true) {



                            val (_, _, userLoginResult) = Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/login/", listOf("username" to "$username" ,"password" to "none")).responseJson()
                            handleLoginResponse(userLoginResult)
                        }
                    }
                }
            }
            else{
                enterWifiTopText.visibility = VISIBLE
                loginUsernameInput.visibility = VISIBLE
                loginUsernameInputLayout.visibility = VISIBLE
                loginPasswordInputLayout.visibility = VISIBLE
                loggingYouInText.visibility = GONE
                enterWifiTopText.text = resources.getString(R.string.you_re_new)
                registerLoginButtonText.text = resources.getString(R.string.register)
                loginPasswordInput.hint = resources.getString(R.string.password_optional)
                Log.d("TESTING", "User registering")
            }


        }
    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.user_fragment_login_register, container, false)
        changeBackground()

        val icon = rootView!!.animated_loading_icon_login
        val iconAvd = icon.drawable as AnimatedVectorDrawable
        iconAvd.registerAnimationCallback(
                object : Animatable2.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable) {
                        iconAvd.start()
                    }
                })
        iconAvd.start()


        rootView!!.loginUsernameInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
            }
            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                username = charSequence.toString()
                when {
                    formattedUserList.contains(username!!) -> {
                        activity?.toast("That username is taken")
                        registerButton.visibility = GONE
                        usernameEntered = false
                    }
                    charSequence.length in 1..12 -> {
                        registerButton.visibility = VISIBLE
                        usernameEntered = true
                    }
                    else -> {
                        registerButton.visibility = GONE
                        usernameEntered = false
                    }
                }
            }
        })

        rootView!!.loginPasswordInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
            }
            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                password = charSequence.toString()

                if ( (usernameEntered && !loggingIn) ||  (charSequence.length in 1..12 && loggingIn)  ){
                    registerButton.visibility = VISIBLE
                }
                else{
                    registerButton.visibility = GONE
                }
            }
        })



        rootView!!.loginPasswordInput.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if ((keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP)) {
                Log.d("TESTING", "User pressed enter")
                dataEntered()
                return@OnKeyListener true
            }
            false
        })


        rootView!!.registerButton.setOnClickListener{
            dataEntered()
        }

        return rootView
    }

    private fun dataEntered() {
        view?.hideKeyboard()
        thread(start = true) {
            load()
            if (loggingIn) {
				Log.d("TESTING", "Logging in with username: $username and Password: $password")

				if (password == null || password == "") {
					Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/login/?format=json", listOf("username" to "$username", "password" to "none")).responseJson { _, _, userLoginResult ->
						handleLoginResponse(userLoginResult)

					}
                } else {
					val requestParams = ArrayList<Pair<String, String>>(2)
					requestParams.add(Pair("username", username!!))
					requestParams.add(Pair("password", password!!))



					val (_, _, userLoginResult) =
							Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/login/", requestParams)
									.responseJson()


					handleLoginResponse(userLoginResult)


                }

            } else {
                if (password == null || password == "") {
                    val (_, _, userRegisterResult) = Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/create_user/?format=json", listOf("username" to "$username", "email" to "no@no.no", "password" to "none")).responseJson()
                    handleRegisterResponse(userRegisterResult)
                } else {
                    val (_, _, userRegisterResult) = Fuel.post("${LoadingActivity.workingIP}:${LoadingActivity.port}/users_api/create_user/?format=json", listOf("username" to "$username", "email" to "yes@yes.yes", "password" to "$password", "has_password" to "yes")).responseJson()
                    handleRegisterResponse(userRegisterResult)
                }
            }
        }
    }

    private fun handleLoginResponse(userLoginResult: Result<Json, FuelError>){

        userLoginResult.success {
            Log.d("TESTING", "Login success")
            val parser = Parser()
            val userLoginResultStringBuilder = StringBuilder(userLoginResult.get().content)
            val userLoginResultJson = parser.parse(userLoginResultStringBuilder) as JsonObject
            val token = userLoginResultJson.string("token")
            val db = AppDatabase.getInstance(this.context!!)
            runBlocking(Dispatchers.Default) {
                db?.loginDAO()?.resetLoggedInUser()
                val loginValues = LoginDB()
                loginValues.loggedIn = 1
                loginValues.token = token!!
                loginValues.user = username!!
                db?.loginDAO()?.updateLoggedInUser(loginValues)
                val loginData = db?.loginDAO()?.getLoggedInUser()
                Log.d("TESTING", "User saved as:" + loginData!![0].user)
            }

            val intent = Intent(this.context!!, LoadingActivity::class.java)
            this.startActivity(intent)
            activity?.finish()
        }

        userLoginResult.failure {
            Log.d("TESTING", "Login failure")
			Log.d("TESTING", it.response.toString())

			if (it.response.responseMessage == "Bad Request"){
                unload()
                this.activity!!.runOnUiThread{
                    this.context?.toast("Password was incorrect")
                }
            }
            else{
                handleFailedConnection(it.exception)
            }
        }
    }

    private fun handleTokenCheck(tokenResult: Result<Json, FuelError>){
        tokenResult.fold(
                success = {
                    Log.d("TESTING", "Login check success")
                    //val parser = Parser()
                    //val userLoginResultstringBuilder = StringBuilder(tokenResult.get().content)
                    //val userLoginResultJson = parser.parse(userLoginResultstringBuilder) as JsonObject
                    //val token = userLoginResultJson.string("key")

                    val db = AppDatabase.getInstance(this.context!!)
                    db?.loginDAO()?.resetLoggedInUser()
                    Log.d("TESTING", "Setting $username as logged in")
                    db?.loginDAO()?.loginExistingUser(username!!)
                    val loginData = db?.loginDAO()?.getLoggedInUser()
                    Log.d("TESTING", "User saved as:" + loginData!![0].user)
                    val intent = Intent(this.context!!, LoadingActivity::class.java)
                    this.startActivity(intent)
                    activity?.finish()
                }
        ) {
            Log.d("TESTING", "Login Check failure $tokenResult")
        }

    }

    private fun handleRegisterResponse(userRegisterResult: Result<Json, FuelError>){
        userRegisterResult.fold(
                success = {
                    Log.d("TESTING", "Register success")

                    val parser = Parser()
                    val userRegisterResultStringBuilder = StringBuilder(userRegisterResult.get().content)
                    val userRegisterResultJson = parser.parse(userRegisterResultStringBuilder) as JsonObject
                    val token = userRegisterResultJson.string("token")

                    val db = AppDatabase.getInstance(this.context!!)
                    db?.loginDAO()?.resetLoggedInUser()

                    val loginValues = LoginDB()
                    loginValues.loggedIn = 1
                    loginValues.token = token!!
                    loginValues.user = username!!
                    db?.loginDAO()?.updateLoggedInUser(loginValues)
                    val loginData = db?.loginDAO()?.getLoggedInUser()
                    Log.d("TESTING", "User saved as:" + loginData!!)
                    val intent = Intent(this.context!!, LoadingActivity::class.java)
                    this.startActivity(intent)
                    activity?.finish()
                }
        ) {
            handleFailedConnection(it.exception)
            Log.d("TESTING", "Register failure $userRegisterResult")
        }

    }

    private fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    private fun load(){
        this.activity!!.runOnUiThread{
            enterWifiTopText.visibility = GONE
            registerButton.visibility = GONE
            loginUsernameInputLayout.visibility = GONE
            loginPasswordInputLayout.visibility = GONE
            animated_loading_icon_login.visibility = VISIBLE
        }
    }

    private fun unload(){
        this.activity!!.runOnUiThread{
            enterWifiTopText.visibility = VISIBLE
            loginPasswordInputLayout.visibility = VISIBLE
            animated_loading_icon_login.visibility = GONE
        }
    }

    private fun handleFailedConnection(exception: Exception){
        // Handle when the app tries to connect to the Brain and fails
        val ex = exception.javaClass.toString()
        if (ex == "class java.net.NoRouteToHostException" ||
                ex == "class java.net.SocketTimeoutException" ||
                ex == "class java.net.ConnectException" ||
                ex == "class java.net.SocketException"){

            Log.d("TESTING", "No route to host on either IP")
            val intent = Intent(this.context!!, CertConfigErrorActivity::class.java)
            intent.putExtra("error",resources.getString(R.string.noConnectionToBrain))
            this.startActivity(intent)
            activity?.finish()
        }
        if (ex == "class javax.net.ssl.SSLHandshakeException" || ex == "class java.security.cert.CertPathValidatorException"){
            Log.d("TESTING", "SSL certs incorrectly configured")
            val intent = Intent(this.context!!, CertConfigErrorActivity::class.java)
            intent.putExtra("error",resources.getString(R.string.sslCertBroken))
            this.startActivity(intent)
            activity?.finish()
        }

        Log.d("TESTING", "Got exception $ex")
    }
    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        var username: String? = null
        var password: String? = null
        fun newInstance(sectionNumber: Int): UserRegisterLogin {
            val fragment = UserRegisterLogin()
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}
