package hudson.pair


//  How Pairing Should Work:
//  01) Check if user is on mobile internet or Wifi
//  02) Enabled WiFi if not enabled
//  03) Make sure user has accepted locations permissions
//  04) Scan for nearby WiFi networks and display a filtered list to the user to select
//  05) Ask the user for the password for their selected Brain
//  06) Use this password to connect to the Brains AP network. Warn the user if they entered the wrong password
//  07) Verify that connection has been established to the Brain by verifying network status then connecting to an API
//  08) Download required certs from the Brain
//  09) Get required IP data for certs
//  10) Disconnect from Brains network by disabling and disconnecting
//  11) Install the required certs from the Brain
//  12) If original network was Wifi then reconnect, else do nothing and mobile data should connect automatically
//  13) Verify that the user is now in fact connected to functional internet
//  14) Verify certs work by connecting to secure API
//
//  Pairing is now complete.

import android.animation.Animator
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.net.*
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.*
import android.view.animation.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.thanosfisherman.wifiutils.WifiUtils
import hudson.R
import hudson.launch.AppDatabase
import hudson.launch.CertConfigErrorActivity
import hudson.launch.EntityStoredBrain
import hudson.launch.LoadingActivity
import hudson.launch.LoadingActivity.Companion.externalIP
import hudson.launch.LoadingActivity.Companion.brainPassword
import hudson.launch.LoadingActivity.Companion.localIP
import hudson.pair.BrainSelectFragmentRecyclerViewAdapter.Companion.selected_ssid
import kotlinx.android.synthetic.main.pair_loading.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*
import kotlin.concurrent.thread



class PairActivity  : AppCompatActivity() {
	private var hudsonNetId: Int? = null
	private var connectedToOriginalNetwork: Boolean = false
	private var downloaded = 0
	private var certFileDownloadAttempts: List<Int> = listOf(0, 0, 0, 0)
	private var hasBegunAuthentication = false
	private var connectedToHudsonNetwork: Boolean = false
	private var ipData: JSONObject? = null
	private var externalCaFile: File? = null
	private var externalClientFile: File? = null

	override fun onConfigurationChanged(newConfig: Configuration){
		super.onConfigurationChanged(newConfig)
		changeBackground()
	}

	private fun changeBackground(){
		if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
			pairLayout.setBackgroundResource(R.drawable.ic_backgroundloadportrait4)
		}
		else{
			pairLayout.setBackgroundResource(R.drawable.ic_backgroundloadlandscape4)
		}
	}
	override fun onCreate(savedInstanceState: Bundle?) {

		super.onCreate(savedInstanceState)
		setContentView(R.layout.pair_loading)
		changeBackground()
		Log.d("TESTING", "Started pair activity")


		//Start loading icon
		val hudsonHatRemove = animated_loading_icon1
		val hudsonBrainPulse = animated_loading_icon2

		val hudsonHatRemoveAvd = hudsonHatRemove.drawable as AnimatedVectorDrawable
		val hudsonBrainPulseAvd = hudsonBrainPulse.drawable as AnimatedVectorDrawable

		hudsonHatRemoveAvd.registerAnimationCallback(
				object : Animatable2.AnimationCallback() {
					override fun onAnimationEnd(drawable: Drawable) {
						hudsonHatRemove.visibility = GONE
						hudsonBrainPulseAvd.start()
					}
				})

		hudsonBrainPulseAvd.registerAnimationCallback(
				object : Animatable2.AnimationCallback() {
					override fun onAnimationStart(drawable: Drawable?) {
						super.onAnimationStart(drawable)
						hudsonHatRemoveAvd.clearAnimationCallbacks()
					}

					override fun onAnimationEnd(drawable: Drawable) {
						hudsonBrainPulseAvd.start()
					}
				})

		Handler().postDelayed({
			hudsonHatRemoveAvd.start()
		}, 1000)



		Log.d("TESTING", "Enabling WiFi")
		WifiUtils.withContext(applicationContext).enableWifi(this::checkIfWifiEnabled)

	}


	private fun hideKeyboard(activity: Activity) {
		val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		var view = activity.currentFocus

		if (view == null) {
			view = View(activity)
		}
		imm.hideSoftInputFromWindow(view.windowToken, 0)
	}


	private fun checkIfWifiEnabled(isSuccess: Boolean) {
		if (isSuccess) {
			Log.d("TESTING", "Wifi Successfully enabled")
			connectToBrainNetwork()
		} else {
			Log.e("TESTING", "Wifi Failed To Enable")
		}
	}


	private fun connectToBrainNetwork() {

		val wifiConfig = WifiConfiguration()
		wifiConfig.SSID = "\"" + selected_ssid!! + "\""
		wifiConfig.preSharedKey = "\"" + brainPassword!! + "\""

		var wifiManager: WifiManager = this.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

		wifiManager = removeExistingNetworkId(wifiManager, "\"" + selected_ssid!! + "\"")
		hudsonNetId = wifiManager.addNetwork(wifiConfig)

		Log.d("TESTING", "Hudson Network ID: " + hudsonNetId.toString())

		wifiManager.disconnect()
		val status = wifiManager.enableNetwork(hudsonNetId!!, true)
		Log.d("TESTING", status.toString())

		wifiManager.reconnect()
		this.registerReceiver(newHudsonBroadcastReceiver, IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION))
	}


	private fun removeExistingNetworkId(wifiManager: WifiManager, SSID: String): WifiManager {
		val configuredNetworks = wifiManager.configuredNetworks
		if (configuredNetworks != null) {
			for (existingConfig in configuredNetworks) {
				if (SSID.equals(existingConfig.SSID, ignoreCase = true)) {
					Log.d("TESTING", "Removing Hudson network preconfigured in device ID: " + existingConfig.networkId)
					wifiManager.removeNetwork(existingConfig.networkId)
				}
			}
		} else {
			Log.d("TESTING", "Found no configured networks")
		}
		return wifiManager
	}


	private var newHudsonBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {

			val notConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)

			if (notConnected) {
				Log.d("TESTING", "Not connected to Hudson Network")
				finish()
				Toast.makeText(context.applicationContext, "The password was incorrect", Toast.LENGTH_SHORT).show()
			} else {

				//Get the SSID of the broadcasted network
				val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
				val info = wifiManager.connectionInfo
				while (info.ssid == null) {
				}
				var broadcastedNetworkSSID = info.ssid

				//Remove quotes around ssid
				broadcastedNetworkSSID = broadcastedNetworkSSID.substring(1, broadcastedNetworkSSID.length - 1)
				Log.d("TESTING", broadcastedNetworkSSID)

				//Get current state of intent (AUTHENTICATING, CONNECTING, CONNECTED...)
				val networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO) as NetworkInfo
				val currentState = networkInfo.detailedState
				Log.d("TESTING", "$broadcastedNetworkSSID is $currentState")
				//Check if the network passed in is the Hudson network
				if (broadcastedNetworkSSID == selected_ssid) {
					Log.d("TESTING", "Network doing action is Hudson network $currentState")
					if (currentState.toString() == "AUTHENTICATING") {
						Log.d("TESTING", "Begun authentication")
						hasBegunAuthentication = true
					}
					if (currentState.toString() == "DISCONNECTED" && hasBegunAuthentication) {
						// If authentication begins and a disconnect follows, it can be assumed
						// that the password was incorrect. End receiver and return to input
						unregisterReceiver(this)
						finish()
						Toast.makeText(context.applicationContext,
								"The password was incorrect", Toast.LENGTH_SHORT).show()
					}
					if (networkInfo.isConnected) {
						// We have authenticated and connected to the Hudson network

						// Now get the networkID of the passed in network and use that to get the
						// just created local Network object.


						val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
								as ConnectivityManager


						// Wait for Hudson network to become connected on device
						while (connectivityManager.activeNetworkInfo == null) {
						}
						while (!connectivityManager.activeNetworkInfo.isConnected) {
						}

						// Specifically bind Wifi connections to the current process
						// so mobile data is not used
						val request = NetworkRequest.Builder()
						request.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)

						val networkCallback = object : ConnectivityManager.NetworkCallback() {
							// Make sure the function is run only once
							var firstAvailable = true

							override fun onAvailable(network: Network?) {
								if (firstAvailable) {
									connectivityManager.bindProcessToNetwork(network)

									connectedToHudsonNetwork = true

									val finishIntent = Intent("finish_activity")
									sendBroadcast(finishIntent)
									Log.d("TESTING", "Finished password input activity")
									run {
										successfulConnect()
									}
									firstAvailable = false
								}
							}
						}
						unregisterReceiver(this)
						connectivityManager.requestNetwork(request.build(), networkCallback)

					}
				}
			}
		}
	}


	private fun successfulConnect() {
		Log.d("TESTING", "Connected to Hudson Network")
		val webSocket = openWebSocketConnectionToBrain()
		sendHudsonWifiCheckMessage(webSocket)
	}


	private fun openWebSocketConnectionToBrain(): WebSocket {
		// Open websocket connection to Brain to easily send and receive data
		// Use a random websocket room to prevent clashing with other users pairing
		val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
		val randomWebsocketRoom = (1..16)
				.map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
				.map(charPool::get)
				.joinToString("")
		val client = OkHttpClient.Builder().readTimeout(3, TimeUnit.SECONDS).build()
		val request = Request.Builder()
				.url("ws://192.168.4.1:17922/ws/$randomWebsocketRoom/")
				.build()
		val wsListener = BrainWebsocket()
		return client.newWebSocket(request, wsListener)
	}


	inner class BrainWebsocket : WebSocketListener() {
		var context:Activity? = null
		override fun onOpen(webSocket: WebSocket, response: Response) {
			Log.d("TESTING", "Websocket connection established")
		}

		override fun onMessage(webSocket: WebSocket?, text: String?) {
			Log.d("TESTING", "Receiving : " + text!!)
			val jsonObject = JSONObject(text)
			if (jsonObject.has("nearByNetworks")) {
				//We received a list of near by networks to display as a list
				displayNearbyNetworksList(jsonObject, webSocket!!)
			}
			if (jsonObject.has("wifiReplaceStatus")) {
				//Received a notice of successful or failed connection to Wifi network
				handleReceivedWifiConnectionStatus(jsonObject, webSocket!!)
			}
			if (jsonObject.has("wifiConnectionCheck")) {
				//Received a message saying whether or not Hudson has local/external connections
				handleReceivedWifiCheck(jsonObject, webSocket!!)
			}
			if (jsonObject.has("IPData")) {
				//Received a message containing Port and SSID data to be saved
				ipData = jsonObject
				reconnectToOriginalNetwork()
			}
		}

		override fun onClosing(webSocket: WebSocket, code: Int, reason: String?) {
			webSocket.close(1000, null)
			Log.d("TESTING", "Closing : $code / $reason")
		}

		override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
			Log.d("TESTING", "Error : " + t!!.message)
			Log.d("TESTING", "Response : $response")
			failedToConnectFirewall()
		}
	}

	private fun failedToConnectFirewall(){
		val intent = Intent(this, CertConfigErrorActivity::class.java)
		intent.putExtra("error", resources.getString(R.string.firewallFailedToConnect))
		this.startActivity(intent)
	}

	private fun sendHudsonWifiCheckMessage(webSocket: WebSocket) {
		val checkingHudsonWifiMessage = JSONObject()
		checkingHudsonWifiMessage.put("checkWifi", true)
		webSocket.send(checkingHudsonWifiMessage.toString())
	}


	private fun handleReceivedWifiCheck(jsonObject: JSONObject, webSocket: WebSocket) {
		// Forward on user to one of three screens depending on Hudson wifi status
		val wifiConnectionCheck = jsonObject.getJSONObject("wifiConnectionCheck")

		val localConnected = wifiConnectionCheck.getBoolean("localConnected")
		if (localConnected) {
			localIP = "https://" + wifiConnectionCheck.getString("localIP")
			// Hudson is part of a Wifi network, let's check external connections
			val externalConnected = wifiConnectionCheck.getBoolean("externalConnected")
			if (externalConnected) {
				// Hudson has full connection, send user to cert downloading
				externalIP = "https://" + wifiConnectionCheck.getString("externalIP")
				wifiSortedOut(webSocket)
			}
		} else {
			// There is no Wifi connection. Send user to Wifi select screen
			sendLookingForNetworksMessageToBrain(webSocket)
		}
	}


	private fun sendLookingForNetworksMessageToBrain(webSocket: WebSocket) {
		// Send a message to the Brain requesting it send a list of scanned WiFi networks
		// back to the device for displaying as a list

		val lookingForNetworksMessage = JSONObject()
		lookingForNetworksMessage.put("lookingForNetworks", true)
		webSocket.send(lookingForNetworksMessage.toString())
	}


	private fun displayNearbyNetworksList(jsonObject: JSONObject, webSocket: WebSocket) {
		// Takes JSON list of WiFi networks and displays them in a RecyclerView

		val nearByNetworks = jsonObject.getString("nearByNetworks")
		val nearByNetworksJSON = JSONArray(nearByNetworks)
		val wifiList = mutableListOf<WifiData>()
		wifiList.clear()
		for (i in 0 until nearByNetworksJSON.length()) {
			val wifiNetwork = nearByNetworksJSON.getJSONObject(i)
			val wifiData = WifiData(wifiNetwork.getString("ssid"),
					wifiNetwork.getBoolean("encrypted"),
					wifiNetwork.getString("encryptionType"))
			wifiList.add(wifiData)
		}
		// We now have a mutable list of JSONObjects with Wifi Data
		wifiListRecyclerView.apply {
			// set a LinearLayoutManager to handle Android
			// RecyclerView behavior
			runOnUiThread {
				Log.d("TESTING", "Thread is " + android.os.Process.getThreadPriority(android.os.Process.myTid()))


				layoutManager = LinearLayoutManager(context)
				wifiListRecyclerView.adapter = WifiNetworkRecyclerViewAdapter(this.context, wifiList) { wifiData ->
					Log.d("TESTING", "Clicked ssid " + wifiData.ssid)
					handleWifiNetworkSelect(wifiData, webSocket)
				}

				fadeOutAnimation(animated_loading_icon2)
				val standByFadeOut = fadeOutAnimation(stand_by_text)
				standByFadeOut.addListener(object : Animator.AnimatorListener {
					override fun onAnimationRepeat(animation: Animator?) {}
					override fun onAnimationCancel(animation: Animator?) {}
					override fun onAnimationStart(animation: Animator?) {}
					override fun onAnimationEnd(animation: Animator) {
						stand_by_text.text = resources.getString(R.string.missingInternet)
						fadeInAnimation(wifiListRecyclerView)
						fadeInAnimation(enterWifiDescription)
						fadeInAnimation(stand_by_text)
					}
				})
			}
		}

	}


	private fun fadeInAnimation(myObject: View): ObjectAnimator {

		myObject.visibility = VISIBLE
		val fadeInAnimator = ObjectAnimator.ofFloat(myObject, ALPHA, 0.0f, 1.0f)
		fadeInAnimator.duration = 600
		fadeInAnimator.start()

		fadeInAnimator.addListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}
			override fun onAnimationCancel(animation: Animator?) {}
			override fun onAnimationStart(animation: Animator?) {}
			override fun onAnimationEnd(animation: Animator) {
			}
		})
		return fadeInAnimator
	}


	private fun fadeOutAnimation(myObject: View): ObjectAnimator {
		val fadeOutAnimator = ObjectAnimator.ofFloat(myObject, ALPHA, 1.0f, 0.0f)
		fadeOutAnimator.duration = 600
		fadeOutAnimator.start()
		fadeOutAnimator.addListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}
			override fun onAnimationCancel(animation: Animator?) {}
			override fun onAnimationStart(animation: Animator?) {}
			override fun onAnimationEnd(animation: Animator) {
				myObject.visibility = GONE
			}
		})
		return fadeOutAnimator

	}


	private fun handleWifiNetworkSelect(wifiData: WifiData, webSocket: WebSocket) {
		// When the user clicks a Wifi Network for the Brain to connect to, fade away the Wifi list
		// and depending on whether the network has encryption, show a password input

		if (wifiData.encrypted) {
			// The selected network has a password
			// Fade out Wifi Network list and fade in password input
			fadeOutAnimation(enterWifiDescription)
			fadeOutAnimation(stand_by_text)
			val wifiListRecyclerViewFadeOut = fadeOutAnimation(wifiListRecyclerView)
			wifiListRecyclerViewFadeOut.addListener(object : Animator.AnimatorListener {
				override fun onAnimationRepeat(animation: Animator?) {}
				override fun onAnimationCancel(animation: Animator?) {}
				override fun onAnimationStart(animation: Animator?) {}
				override fun onAnimationEnd(animation: Animator) {
					stand_by_text.text =
							String.format(
									resources.getString(R.string.enterWifiPassword), wifiData.ssid)
					fadeInAnimation(wifiPasswordInputLayout)
					fadeInAnimation(stand_by_text)
					fadeInAnimation(goBackCardViewLayout)
				}
			})

			// Add password listener which shows a confirm button when an 8 character password
			// is inputted
			wifiPasswordInput.addTextChangedListener(object : TextWatcher {
				override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
				}

				override fun afterTextChanged(s: Editable?) {
				}

				override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
					Log.d("TESTING", "Entered text")

					if (charSequence.length >= 8) {
						confirmWifiPasswordButtonLayout.alpha = 1.0F
						confirmWifiPasswordButtonLayout.visibility = VISIBLE
					} else {
						confirmWifiPasswordButtonLayout.visibility = GONE
					}
				}
			})

			confirmWifiPasswordButton.setOnClickListener {
				// Confirm password button was clicked, send data to Brain
				sendWifiDataToBrain(wifiData, webSocket)
			}

			goBackButton.setOnClickListener {
				// Back button was clicked

				returnToWifiNetworkSelect()
			}
			// Confirm password with enter key
			wifiPasswordInput.setOnKeyListener(OnKeyListener { _, keyCode, event ->
				if ((keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP)) {
					if (wifiPasswordInput.text!!.length >= 8) {
						Log.d("TESTING", "Hit enter and password >= 8 chars")
						sendWifiDataToBrain(wifiData, webSocket)
						return@OnKeyListener true
					} else {
						return@OnKeyListener false
					}

				}
				false
			})
			Log.d("TESTING", "wifiListRecyclerView Visibility = " + wifiListRecyclerView.visibility)


		}

	}


	private fun returnToWifiNetworkSelect() {
		// Handle when a user clicks 'Go Back' on the password input screen for a Wifi Network
		// Fade out the password fields and fade back in the Wifi List
		fadeOutAnimation(wifiPasswordInputLayout)
		fadeOutAnimation(stand_by_text)

		val wifiListRecyclerViewFadeOut = fadeOutAnimation(goBackCardViewLayout)
		wifiListRecyclerViewFadeOut.addListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}
			override fun onAnimationCancel(animation: Animator?) {}
			override fun onAnimationStart(animation: Animator?) {}
			override fun onAnimationEnd(animation: Animator) {
				wifiPasswordInput.text!!.clear()
				stand_by_text.text = resources.getString(R.string.missingInternet)
				fadeInAnimation(enterWifiDescription)
				fadeInAnimation(stand_by_text)
				fadeInAnimation(wifiListRecyclerView)
			}
		})

	}


	private fun sendWifiDataToBrain(wifiData: WifiData, webSocket: WebSocket) {
		// Update UI
		hideKeyboard(this@PairActivity)
		if (wifiData.encrypted) {
			uiSendWifiToBrainFromPasswordInput()
		} else {
			uiSendWifiToBrainFromWifiList()
		}

		// Send ssid, psk and country code to the Brain for logging in with
		val countryCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			this.resources.configuration.locales.get(0)
		} else {
			@Suppress("DEPRECATION")
			this.resources.configuration.locale
		}


		val wifiDetails = JSONObject()
		wifiDetails.put("ssid", wifiData.ssid)
		wifiDetails.put("psk", wifiPasswordInput.text)
		wifiDetails.put("countryCode", countryCode)
		val wifiDetailsHolder = JSONObject()
		wifiDetailsHolder.put("wifiDetails", wifiDetails)

		webSocket.send(wifiDetailsHolder.toString())

	}


	private fun uiSendWifiToBrainFromPasswordInput() {
		//When sending Wifi data to Brain, fade out password input and fade in spinning Hudson

		fadeOutAnimation(wifiPasswordInputLayout)
		fadeOutAnimation(stand_by_text)
		fadeOutAnimation(confirmWifiPasswordButtonLayout)

		val wifiListRecyclerViewFadeOut = fadeOutAnimation(goBackCardViewLayout)
		wifiListRecyclerViewFadeOut.addListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}
			override fun onAnimationCancel(animation: Animator?) {}
			override fun onAnimationStart(animation: Animator?) {}
			override fun onAnimationEnd(animation: Animator) {
				stand_by_text.text = resources.getString(R.string.sendingWifiToBrain)
				fadeInAnimation(animated_loading_icon2)
				fadeInAnimation(stand_by_text)
			}
		})
	}


	private fun uiSendWifiToBrainFromWifiList() {
		//When sending Wifi data to Brain, fade out Wifi List and fade in spinning Hudson

		fadeOutAnimation(wifiListRecyclerView)
		fadeOutAnimation(stand_by_text)
		fadeOutAnimation(enterWifiDescription)

		val wifiListRecyclerViewFadeOut = fadeOutAnimation(stand_by_text)
		wifiListRecyclerViewFadeOut.addListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}
			override fun onAnimationCancel(animation: Animator?) {}
			override fun onAnimationStart(animation: Animator?) {}
			override fun onAnimationEnd(animation: Animator) {
				stand_by_text.text = resources.getString(R.string.sendingWifiToBrain)
				fadeInAnimation(animated_loading_icon2)
				fadeInAnimation(stand_by_text)
			}
		})
	}


	private fun handleReceivedWifiConnectionStatus(jsonObject: JSONObject, webSocket: WebSocket) {
		// When we receive a websocket message from the Brain stating the the sent Wifi data
		// connected or not, or whether the port forwarded successfully, we change the screen
		// accordingly
		val wifiReplaceStatus = jsonObject.getJSONObject("wifiReplaceStatus")

		val wifiConnected = wifiReplaceStatus.getBoolean("wifiConnected")
		val portForwarded = wifiReplaceStatus.getBoolean("portForwarded")
		val error = wifiReplaceStatus.getString("error")


		if (!wifiConnected) {
			returnToWifiListDueToError(error)
		} else {
			// Wifi Connected successfully
			externalIP = wifiReplaceStatus.getString("externalIP")
			localIP = wifiReplaceStatus.getString("localIP")
			if (!portForwarded) {
				switchToPortForwardGuide(wifiReplaceStatus, webSocket)
			} else {
				wifiSortedOut(webSocket)
			}
		}
	}


	private fun returnToWifiListDueToError(error: String) {
		// The Wifi failed to connect due to an incorrect password / other error
		runOnUiThread {
			fadeOutAnimation(animated_loading_icon2)
			val standByFadeOut = fadeOutAnimation(stand_by_text)
			standByFadeOut.addListener(object : Animator.AnimatorListener {
				override fun onAnimationRepeat(animation: Animator?) {}
				override fun onAnimationCancel(animation: Animator?) {}
				override fun onAnimationStart(animation: Animator?) {}
				override fun onAnimationEnd(animation: Animator) {
					stand_by_text.text = resources.getString(R.string.missingInternet)
					wifiPasswordInput.text!!.clear()
					fadeInAnimation(wifiListRecyclerView)
					fadeInAnimation(enterWifiDescription)
					fadeInAnimation(stand_by_text)
					this@PairActivity.toast(error)
				}
			})
		}

	}


	private fun switchToPortForwardGuide(wifiReplaceStatus: JSONObject, webSocket: WebSocket) {
		// Port wasn't forwarded and user needs to be told how to do this manually.
		Log.d("TESTING", "Thread is " + android.os.Process.getThreadPriority(android.os.Process.myTid()))

		val ssid = wifiReplaceStatus.getString("ssid")
		val gatewayIP = wifiReplaceStatus.getString("gatewayIP")
		val port = wifiReplaceStatus.getInt("port")

		runOnUiThread {
			okayPortForwardButton.setOnClickListener {
				sendHudsonWifiCheckMessage(webSocket)
			}

			portForwardGuide.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
				Html.fromHtml(getString(R.string.portForwardGuide, ssid, gatewayIP, port),
						Html.FROM_HTML_MODE_LEGACY)
			} else {
				@Suppress("DEPRECATION")
				Html.fromHtml(getString(R.string.portForwardGuide, ssid, gatewayIP, port))
			}



			fadeOutAnimation(animated_loading_icon2)
			val standByFadeOut = fadeOutAnimation(stand_by_text)
			standByFadeOut.addListener(object : Animator.AnimatorListener {
				override fun onAnimationRepeat(animation: Animator?) {}
				override fun onAnimationCancel(animation: Animator?) {}
				override fun onAnimationStart(animation: Animator?) {}
				override fun onAnimationEnd(animation: Animator) {
					stand_by_text.text = resources.getString(R.string.noOutsideConnection)
					fadeInAnimation(portForwardGuideCardView)
					fadeInAnimation(okayPortForwardButtonLayout)
					fadeInAnimation(stand_by_text)
				}
			})
		}

	}


	private fun wifiSortedOut(webSocket: WebSocket) {
		Log.d("TESTING", "Wifi is all sorted out")
		runOnUiThread{
			if (portForwardGuideCardView.visibility == VISIBLE) {
				Log.d("TESTING", "Port Forward guide is visible")
				fadeOutAnimation(portForwardGuideCardView)
			}
			if (okayPortForwardButtonLayout.visibility == VISIBLE) {
				Log.d("TESTING", "Port Forward Button is visible")
				fadeOutAnimation(okayPortForwardButtonLayout)
			}

			val standByFadeOut = fadeOutAnimation(stand_by_text)
			standByFadeOut.addListener(object : Animator.AnimatorListener {
				override fun onAnimationRepeat(animation: Animator?) {}
				override fun onAnimationCancel(animation: Animator?) {}
				override fun onAnimationStart(animation: Animator?) {}
				override fun onAnimationEnd(animation: Animator) {
					stand_by_text.text = resources.getString(R.string.wifiDone)
					if (animated_loading_icon2.visibility == GONE){
						fadeInAnimation(animated_loading_icon2)
					}
					fadeInAnimation(stand_by_text)
				}
			})
		}

		GlobalScope.launch{
			//Download certs
			downloadCerts(webSocket)
		}
	}


	private fun downloadCerts(webSocket: WebSocket) {


		val keyDirectory = this.getDir("keys", MODE_PRIVATE)
		downloaded = 0


		downloadCertFile(webSocket, "externalCa.crt", keyDirectory, 0)
		downloadCertFile(webSocket, "externalClient.p12", keyDirectory, 1)

	}


	private fun downloadCertFile(webSocket: WebSocket, file: String, keyDirectory: File, listValue: Int) {
		Thread.sleep(1000)
		Log.d("TESTING", "Beginning Client File download")

		Fuel.download("http://192.168.4.1:17922/media/$file").destination { _, _ -> File(keyDirectory, file) }.response { _, _, result ->
			result.fold({
				downloaded++
				Log.d("TESTING", "Downloaded file $downloaded: $file")
				if (downloaded == 2){
					Log.d("TESTING", "All cert downloads complete")

					externalCaFile = File(keyDirectory, "externalCa.crt")
					externalClientFile = File(keyDirectory, "externalClient.p12")
//					localCaFile = File(keyDirectory, "localCa.crt")
//					localClientFile = File(keyDirectory, "localClient.p12")
					sendGetPortDataMessage(webSocket)
				}
			},
					{ error ->
						Log.d("TESTING", "Cert download error: $error")
						Thread.sleep(1000)
						if (certFileDownloadAttempts[listValue] < 5) {
							downloadCertFile(webSocket, file, keyDirectory, listValue)
						}
						if (certFileDownloadAttempts[listValue] >= 5) {
							//The downloads failed
							val intent = Intent(this, CertConfigErrorActivity::class.java)
							intent.putExtra("error", resources.getString(R.string.requiredFileDownloadFailed))
							this.startActivity(intent)
							finish()
						}

					})
		}
	}


	private fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


	private fun sendGetPortDataMessage(webSocket: WebSocket) {
		// Send a message to the Brain that we want data about Ports
		val ipDataMessage = JSONObject()
		ipDataMessage.put("getIPData", true)
		webSocket.send(ipDataMessage.toString())
		Log.d("TESTING", "Sending request for IP/Port data")
	}

	private fun reconnectToOriginalNetwork() {


		this.runOnUiThread {
			stand_by_text.text = resources.getString(R.string.disconnecting)
		}

		Log.d("TESTING", "Getting ready to connect to original network")


		val wifiManager = this.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

		wifiManager.disconnect()
		wifiManager.disableNetwork(hudsonNetId!!)
		wifiManager.removeNetwork(hudsonNetId!!)
		connectedToOriginalNetwork = false
		if (startedWithWifiorMobile == 1) {

			wifiManager.enableNetwork(startingNetID!!, true)
			this.registerReceiver(originalNetworkBroadcastReceiver,
					IntentFilter((WifiManager.NETWORK_STATE_CHANGED_ACTION)))
		} else {
			handleCAInstall()
			saveBrain()
		}

	}


	private var originalNetworkBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {

			val networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO) as NetworkInfo

			Log.d("TESTING", networkInfo.toString())


			if (!networkInfo.isConnected) {
				Log.d("TESTING", "Not connected to Original Network")
			} else {

				val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
						as ConnectivityManager


				// Wait for Hudson network to become connected on device
				while (connectivityManager.activeNetworkInfo == null) {
				}
				while (!connectivityManager.activeNetworkInfo.isConnected) {
				}

				val wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
				val connectionInfo = wifiManager.connectionInfo
				val currentNetID = connectionInfo.networkId


				if (currentNetID == startingNetID && !connectedToOriginalNetwork) {
					connectedToOriginalNetwork = true
					Log.d("TESTING", "Unbinding network")
					connectivityManager.bindProcessToNetwork(null)
					thread(start = true) {
						handleCAInstall()
						saveBrain()
					}
					unregisterReceiver(this)
				}
			}
		}
	}


	private fun handleCAInstall() {
		Log.d("TESTING", "Connected to original network")

		this.runOnUiThread {
			stand_by_text.text = resources.getString(R.string.configuring_security)
		}

		if (originalNetworkBroadcastReceiver.isOrderedBroadcast) {
			this.unregisterReceiver(originalNetworkBroadcastReceiver)
		}
		installCert(externalCaFile!!, externalClientFile!!)
		//installCert(localCaFile!!, localClientFile!!)


	}


	private fun installCert(certFile: File, clientFile: File) {

		val ks = KeyStore.getInstance("pkcs12")
		val fis = FileInputStream(clientFile)
		val pwd = brainPassword?.toCharArray()
		ks.load(fis, pwd)
		val kmf = KeyManagerFactory.getInstance("PKIX")
		kmf.init(ks, pwd)

		val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
		val caInput: InputStream = BufferedInputStream(FileInputStream(certFile))
		val ca: X509Certificate = caInput.use { cf.generateCertificate(it) as X509Certificate }
		System.out.println("ca=" + ca.subjectDN)

		val keyStoreType = KeyStore.getDefaultType()
		val keyStore = KeyStore.getInstance(keyStoreType).apply {
			load(null, null)
			setCertificateEntry("ca", ca)
		}

		val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm()
		val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm).apply { init(keyStore) }

		HttpsURLConnection.setDefaultHostnameVerifier(NullHostNameVerifier())
		val context = SSLContext.getInstance("TLS").apply { init(kmf.keyManagers, tmf.trustManagers, null) }
		sslSocket = context.socketFactory
		HttpsURLConnection.setDefaultSSLSocketFactory(context.socketFactory)

		FuelManager.instance.hostnameVerifier = NullHostNameVerifier()
		FuelManager.instance.socketFactory = context.socketFactory
		LoadingActivity.sslSocket = context.socketFactory

	}


	inner class NullHostNameVerifier : HostnameVerifier {

		override fun verify(hostname: String, session: SSLSession): Boolean {
			Log.i("RestUtilImpl", "Approving certificate for $hostname")
			return externalIP!!.contains(hostname) || localIP!!.contains(hostname)
		}

	}


	private fun saveBrain() {

		val ipDataJSON = ipData!!.getJSONObject("IPData")

		val port = ipDataJSON.getInt("port")
		val ssid = ipDataJSON.getString("ssid")
		val brainID = ipDataJSON.getString("hudsonID")

		thread(start = true) {
			val db = AppDatabase.getInstance(this)
			val entityStoredBrain = EntityStoredBrain()
			entityStoredBrain.brainID = brainID
			entityStoredBrain.brainName = selected_ssid!!
			entityStoredBrain.externalIP = externalIP!!
			entityStoredBrain.localIP = localIP!!
			entityStoredBrain.brainSSID = ssid
			entityStoredBrain.brainPassword = brainPassword!!
			entityStoredBrain.port = port

			db?.storedBrainDAO()?.insertBrain(entityStoredBrain)
			enterNextZone()
		}

	}


	private fun enterNextZone() {
		this.runOnUiThread {
			val fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out)


			animated_loading_icon1.visibility = GONE

			val fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
			pairing_complete_text.startAnimation(fadeInAnimation)
			fadeInAnimation.setAnimationListener(object : Animation.AnimationListener {
				override fun onAnimationStart(animation: Animation?) {
					pairing_complete_text.alpha = 1.0F
				}

				override fun onAnimationRepeat(animation: Animation?) {}
				override fun onAnimationEnd(arg0: Animation) {}
			})

			stand_by_text.startAnimation(fadeOutAnimation)
			fadeOutAnimation.setAnimationListener(object : Animation.AnimationListener {
				override fun onAnimationStart(animation: Animation?) {
					stand_by_text.alpha = 0.0F
				}

				override fun onAnimationRepeat(animation: Animation?) {}
				override fun onAnimationEnd(arg0: Animation) {}
			})
			Thread.sleep(1000)

			val intent = Intent(this, LoadingActivity::class.java)
			startActivity(intent)
			finish()

		}

	}


	override fun onPause() {
		super.onPause()
		finish()

		if (originalNetworkBroadcastReceiver.isOrderedBroadcast) {
			this.unregisterReceiver(originalNetworkBroadcastReceiver)
		}
	}


	companion object {
		var startingNetID: Int? = null
		var startedWithWifiorMobile: Int = 0
		var sslSocket: SSLSocketFactory? = null
	}
}

