<i>This is an overview of the Hudson Android App.
For information on the Hudson system as a whole,
[see here](https://gitlab.com/hudson-by-starman/hudson-meta)</i>

# Hudson Android

The android app currently has an animated intro using animated SVG's.

The user is given privacy warnings then directed to accept location requests which is needed to scan
Wifi networks.

The app scans nearby WiFi networks for local Brain devices.

The user selects their device and enters its password.

The app connects to the Brains AP network.

If the Brain does not have its own Wifi connection it will will scan for nearby networks.

The list of scanned networks is sent back to the phone so it can be displayed as a list.

The user selects a network that it wishes for the Brain to connect to, and provides a password if
required.

The Brain attempts to connect to this network. If the connection fails the user is told what the
problem is (Incorrect password, bad network) and they're returned to the selection screen.

If successful, the Brain gets its own External, Local and Gateway IPs to generate TLS certificated.

It also attempts to automatically forward the randomly generated port that the Brain uses for
external connections.

If this fails the user is shown a screen that forwarding has failed and external connections will
not work until they manually forward the port.

The Brain sends the device the required certificates(a standard TLS certificate and a client
certificate) for secure connection when it's ready.

These certificates are only accessible to devices that are inside the Brains access point network,
this means that the user requires the Brains password to access any secure data on the Brain. After
collecting the required certificates the app disconnects from the Brain and reconnects to the
previous network. It then installs the certificates within the app and tests a secure connection
to the TLS encrypted website that the Brain devices hosts which is open to WAN.

The android and Brain are now considered securely paired.



## Here's an example:

![](https://gitlab.com/hudson-by-starman/hudson-meta/raw/master/media/video/appDemo.mp4)
